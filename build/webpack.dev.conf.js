'use strict'

const fs = require('fs')
const path = require('path')
const utils = require('./utils')
const webpack = require('webpack')
const config = require('../config')
const merge = require('webpack-merge')
const baseWebpackConfig = require('./webpack.base.conf')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')

// switch this number to change the used whitelabel config
// 0 = seestadt.city
// 1 = stadtkatalog.org
// 2 = seestadt.bot
const configIndex = 2;

const label = baseWebpackConfig[configIndex].label;
delete baseWebpackConfig[configIndex].label;

// add hot-reload related code to entry chunks
Object.keys(baseWebpackConfig[configIndex].entry).forEach(function (name) {
  baseWebpackConfig[configIndex].entry[name] = ['./build/dev-client'].concat(baseWebpackConfig[configIndex].entry[name])
})

module.exports = merge(baseWebpackConfig[configIndex], {
  module: {
    rules: utils.styleLoaders({ sourceMap: config.dev.cssSourceMap })
  },
  // cheap-module-eval-source-map is faster for development
  devtool: '#cheap-module-eval-source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env': config.dev.env
    }),
    // https://github.com/glenjamin/webpack-hot-middleware#installation--usage
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    // https://github.com/ampedandwired/html-webpack-plugin
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'index-' + label + '.html',
      inject: true
    }),
    new FriendlyErrorsPlugin()
  ]
})
