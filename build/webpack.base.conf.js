'use strict';

const path = require('path');
const utils = require('./utils');
const webpack = require('webpack');
const config = require('../config');
const vueLoaderConfig = require('./vue-loader.conf');

function resolve (dir) {
  return path.join(__dirname, '..', dir)
}

module.exports = [
  {
    label: "seestadtcity",
    entry: {
      app: './src/main-seestadtcity.js'
    },
    output: {
      path: process.env.NODE_ENV === 'production'
        ? path.join(config.build.assetsRoot, "seestadtcity")
        : config.build.assetsRoot,

      filename: '[name].js',
      publicPath: process.env.NODE_ENV === 'production'
        ? config.build.assetsPublicPath
        : config.dev.assetsPublicPath
    },
    resolve: {
      extensions: ['.js', '.vue', '.json'],
      alias: {
        '@': resolve('src'),
        'SiteVariables': path.join(__dirname, '..', 'src', 'assets', 'seestadtcity', 'siteVariables.scss')
      }
    },
    module: {
      rules: [
        {
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          enforce: 'pre',
          include: [resolve('src'), resolve('test')],
          options: {
            formatter: require('eslint-friendly-formatter')
          }
        },
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          options: vueLoaderConfig
        },
        {
          test: /\.js$/,
          loader: 'babel-loader',
          include: [resolve('src'), resolve('test')]
        },
        {
          test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: utils.assetsPath('img/[name].[hash:7].[ext]')
          }
        },
        {
          test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: utils.assetsPath('media/[name].[hash:7].[ext]')
          }
        },
        {
          test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: utils.assetsPath('fonts/[name].[hash:7].[ext]')
          }
        }
      ]
    },
    plugins: [
      new webpack.DefinePlugin({
        FRONTPAGE_COMPONENT: JSON.stringify("../components/FrontPageSeestadtCity.vue"),
        STADTKATALOG_CONFIG: JSON.stringify({
          siteName: "Seestadt.city",
          geoFenceName: "seestadt",
          mapZoom: 16,
          mapCenter: [48.224479,16.502969],
          clusterOptions: {
            maxClusterRadius: 20
          }
        })
      })
    ]
  },
  {
    label: "stadtkatalog",
    entry: {
      app: './src/main-stadtkatalog.js'
    },
    output: {
      path: process.env.NODE_ENV === 'production'
        ? path.join(config.build.assetsRoot, "stadtkatalog")
        : config.build.assetsRoot,

      filename: '[name].js',
      publicPath: process.env.NODE_ENV === 'production'
        ? config.build.assetsPublicPath
        : config.dev.assetsPublicPath
    },
    resolve: {
      extensions: ['.js', '.vue', '.json'],
      alias: {
        '@': resolve('src'),
        'SiteVariables': path.join(__dirname, '..', 'src', 'assets', 'stadtkatalog', 'siteVariables.scss')
      }
    },
    module: {
      rules: [
        {
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          enforce: 'pre',
          include: [resolve('src'), resolve('test')],
          options: {
            formatter: require('eslint-friendly-formatter')
          }
        },
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          options: vueLoaderConfig
        },
        {
          test: /\.js$/,
          loader: 'babel-loader',
          include: [resolve('src'), resolve('test')]
        },
        {
          test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: utils.assetsPath('img/[name].[hash:7].[ext]')
          }
        },
        {
          test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: utils.assetsPath('media/[name].[hash:7].[ext]')
          }
        },
        {
          test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: utils.assetsPath('fonts/[name].[hash:7].[ext]')
          }
        }
      ]
    },
    plugins: [
      new webpack.DefinePlugin({
        FRONTPAGE_COMPONENT: JSON.stringify("../components/FrontPageStadtKatalog.vue"),
        STADTKATALOG_CONFIG: JSON.stringify({
          siteName: "StadtKatalog",
          geoFenceName: null,
          mapZoom: 12,
          mapCenter: [48.208417,16.372472],
          clusterOptions: {
            maxClusterRadius: 50
          }
        })
      })
    ]
  },
  {
    label: "seestadtbot",
    entry: {
      app: './src/main-seestadtbot.js'
    },
    output: {
      path: process.env.NODE_ENV === 'production'
        ? path.join(config.build.assetsRoot, "seestadtbot")
        : config.build.assetsRoot,

      filename: '[name].js',
      publicPath: process.env.NODE_ENV === 'production'
        ? config.build.assetsPublicPath
        : config.dev.assetsPublicPath
    },
    resolve: {
      extensions: ['.js', '.vue', '.json'],
      alias: {
        '@': resolve('src'),
        'SiteVariables': path.join(__dirname, '..', 'src', 'assets', 'seestadtbot', 'siteVariables.scss')
      }
    },
    module: {
      rules: [
        {
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          enforce: 'pre',
          include: [resolve('src'), resolve('test')],
          options: {
            formatter: require('eslint-friendly-formatter')
          }
        },
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          options: vueLoaderConfig
        },
        {
          test: /\.js$/,
          loader: 'babel-loader',
          include: [resolve('src'), resolve('test')]
        },
        {
          test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: utils.assetsPath('img/[name].[hash:7].[ext]')
          }
        },
        {
          test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: utils.assetsPath('media/[name].[hash:7].[ext]')
          }
        },
        {
          test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: utils.assetsPath('fonts/[name].[hash:7].[ext]')
          }
        }
      ]
    },
    plugins: [
      new webpack.DefinePlugin({
        FRONTPAGE_COMPONENT: JSON.stringify("../components/FrontPageSeestadtBot.vue"),
        STADTKATALOG_CONFIG: JSON.stringify({
          siteName: "Seestadt.bot",
          geoFenceName: "seestadt",
          mapZoom: 16,
          mapCenter: [48.224479,16.502969],
          clusterOptions: {
            maxClusterRadius: 20
          },
          botUrl: process.env.NODE_ENV === "production"
            ? "https://stadtbots.appspot.com/web/seestadtbot"
            : "https://stadtbots-dev-09091803.eu.ngrok.io/web/seestadtbot"
        })
      })
    ]
  },
]
