import "whatwg-fetch";
import * as moment from "moment";

import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

const API_URL = process.env.STK_API_ENDPOINT;
const {geoFenceName} = STADTKATALOG_CONFIG;

export default new Vuex.Store({
  state: {
    loading: false,
    query: "",
    querySize: 10,
    queryFrom: 0,
    hits: [],
    totalHits: -1,
    geoFenceName: geoFenceName,
    geoJson: {},
    geoJsonTagFilter: null,
    promotionDays: 7,
    promotionEntries: [],
    overview: {
      entries: [],
      size: 25,
      page: 0,
      total: 0
    }
  },
  getters: {
    standardParameters: state => {
      return [
        "size=" + state.querySize,
        "from=" + state.queryFrom,
        "validAfter=" + (new Date()).toISOString()
      ].join("&");
    },
    updateUrl: (state, getters) =>  {
      return API_URL + "/search/fulltext?q=" + encodeURIComponent(state.query) + "&" + getters.standardParameters +
        (state.geoFenceName !== null ? `&geoFenceName=${state.geoFenceName}` : "")
    },
    geoJsonUrl: (state, getters) =>  {
      return API_URL + "/export/geojson?size=1000" + (state.geoFenceName !== null ? "&geoFenceName=" + state.geoFenceName : "");
    },
    promotionsUrl: (state, getters) => {
      return API_URL + "/search/promotions?size=10&" +
        "promotionStartAfter=" + encodeURIComponent(moment().startOf("day").format()) +
        "&promotionStartBefore=" + encodeURIComponent(moment().add(state.promotionDays, "days").endOf("day").format()) +
        (state.geoFenceName !== null ? `&geoFenceName=${state.geoFenceName}` : "");
    },
    entryListUrl: (state, getters) =>  {
      return `${API_URL}/export/json?sortField=name&sortOrder=asc&size=${state.overview.size}&from=${state.overview.page * state.overview.size}` +
        (state.geoFenceName !== null ? `&geoFenceName=${state.geoFenceName}` : "");
    },
    currentPage: (state) => {
      return Math.floor(state.queryFrom / state.querySize) + 1;
    },
    totalPages: (state) => {
      return Math.ceil(state.totalHits / state.querySize);
    },
    hasNextPage: (state) => {
      return state.queryFrom + state.querySize < state.totalHits;
    },
    hasPrevPage: (state) => {
      return state.queryFrom > 0;
    },
    hasNextOverviewPage: (state) => {
      return state.overview.total > (state.overview.size * state.overview.page) + state.overview.size;
    },
    hasPrevOverviewPage: (state) => {
      return state.overview.page > 0;
    }
  },
  actions: {
    updateHits ({ commit }) {
      commit("setLoading", true);
      return new Promise((resolve, reject) => {
        fetch(this.getters.updateUrl)
          .then(function (response) {
            if(response.ok) {
              return response.json();
            }

            throw new Error("Network error.");
          }).then((json) => {
            commit("setResultState", {
              totalHits: json.totalHits,
              hits: json.hits
            });
            commit("setLoading", false);
            resolve();
          }).catch((ex) => {
            commit("setLoading", false);
            console.error(ex);
            commit("setResultState", {
              totalHits: -1,
              hits: []
            });
            reject(ex);
          });
      });
    },
    updateGeoJson ({ commit }) {
      commit("setLoading", true);
      return new Promise((resolve, reject) => {
        fetch(this.getters.geoJsonUrl)
          .then(function (response) {
            commit("setLoading", false);
            return response.json();
          }).then((json) => {
            if (this.state.geoJsonTagFilter !== null) {
              json.features = json.features.filter(feature => feature.properties.tags.indexOf(this.state.geoJsonTagFilter) >= 0)
            }

            commit("setGeoJson", {
              object: json
            });
            resolve();
          }).catch((ex) => {
            console.error(ex);
            commit("setGeoJson", {
              object: {}
            });
            reject();
          });
      });
    },
    updatePromotionEntries({ commit }) {
      return new Promise((resolve, reject) => {
        fetch(this.getters.promotionsUrl)
          .then(function (response) {
            return response.json();
          })
          .then((json) => {
            commit("setPromotionEntries", {
              entries: json.hits
            });
            resolve();
          }).catch((ex) => {
            console.error(ex);
            commit("setPromotionEntries", {
              entries: []
            });
            reject();
          });
      });
    },
    updateOverview({ commit }) {
      return new Promise((resolve, reject) => {
        fetch(this.getters.entryListUrl)
          .then(function (response) {
            commit("setOverviewTotal", {
              count: Number.parseInt(response.headers.get("Export-Total-Results"))
            });
            return response.json();
          })
          .then((json) => {
            commit("setOverviewEntries", {
              entries: json
            });
            resolve();
          }).catch((ex) => {
          console.error(ex);
          commit("setOverviewEntries", {
            entries: []
          });
          reject();
        });
      })
    },
    prevOverviewPage({ commit }) {
      if (this.getters.hasPrevOverviewPage) {
        commit("prevOverviewPage");
      }
    },
    nextOverviewPage({ commit }) {
      if (this.getters.hasNextOverviewPage) {
        commit("nextOverviewPage");
      }
    }
  },
  mutations: {
    resetQueryStates(state) {
      state.loading = false;
      state.query = "";
      state.queryFrom = 0;
      state.totalHits = -1;
      state.hits = [];
    },
    setLoading (state, loading) {
      state.loading = loading === true;
    },
    setQuery (state, query) {
      state.query = query;
    },
    setResultState (state, {hits, totalHits}) {
      state.hits = hits;
      state.totalHits = totalHits;
    },
    setQueryFrom (state, queryFrom) {
      state.queryFrom = queryFrom;
    },
    setGeoJsonTagFilter (state, filter) {
      state.geoJsonTagFilter = filter;
    },
    setGeoJson (state, { object }) {
      state.geoJson = object;
    },
    setPromotionEntries (state, { entries }) {
      state.promotionEntries = entries;
    },
    setOverviewTotal (state, { count }) {
      state.overview.total = count;
    },
    setOverviewEntries (state, { entries }) {
      state.overview.entries = entries;
    },
    nextOverviewPage (state) {
      state.overview.page ++;
    },
    prevOverviewPage (state) {
      state.overview.page --;
    }
  }
});
