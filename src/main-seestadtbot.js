import "babel-polyfill";

// step 1: prepare everything for tracking
const _paq = window._paq = (_paq || []);
_paq.push(["setCookieDomain", "*.seestadt.bot"]);
_paq.push(["enableHeartBeatTimer", (process.env.NODE_ENV === "production" ? 7 : 45)]);

(function () {
  const u = "https://stats.pnp-appserver.xyz/";
   _paq.push(['setTrackerUrl', u+'matomo.php']);
  _paq.push(['setSiteId', (process.env.NODE_ENV === "production" ? "2" : "3")]);
  const d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
  g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
})();

// step 2: initialize application
import Vue from "vue";
import App from "./AppSeestadtBot";
import router from "./router";
import VueMq from "vue-mq";

Vue.use(VueMq, {
  breakpoints: { // default breakpoints - customize this
    mobile:  590,
    tablet:  1020,
    desktop: Infinity
  },
  defaultBreakpoint: "mobile" // customize this for SSR
});

Vue.config.productionTip = false;

// maps + webpack fix for leaflet
import L from "leaflet";
delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});

import Vue2Leaflet from "vue2-leaflet";
import Vue2LeafletMarkerCluster from "vue2-leaflet-markercluster";
Vue.component("v-map", Vue2Leaflet.Map);
Vue.component("v-tilelayer", Vue2Leaflet.TileLayer);
Vue.component("v-marker", Vue2Leaflet.Marker);
Vue.component("v-popup", Vue2Leaflet.Popup);
Vue.component("v-geo-json", Vue2Leaflet.GeoJSON);
Vue.component("v-marker-cluster", Vue2LeafletMarkerCluster);

import store from "./store";
store.state.promotionDays = 2;

/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  store,
  render: h => h(App)
});
