import L from "leaflet";
import "leaflet-extra-markers";
import "leaflet-extra-markers/dist/js/leaflet.extra-markers.min";
import "leaflet-extra-markers/dist/css/leaflet.extra-markers.min.css";

export default {
  defaultIcon: L.ExtraMarkers.icon({
    icon: 'ion-android-locate',
    markerColor: 'cyan',
    shape: 'square'
  }),
  educationIcon: L.ExtraMarkers.icon({
    icon: 'ion-university',
    markerColor: 'green-light',
    shape: 'square'
  }),
  restaurantIcon: L.ExtraMarkers.icon({
    icon: 'ion-android-restaurant',
    markerColor: 'orange',
    shape: 'square'
  }),
  doctorIcon: L.ExtraMarkers.icon({
    icon: 'ion-ios-medkit-outline',
    markerColor: 'violet',
    shape: 'square'
  }),
  healthIcon: L.ExtraMarkers.icon({
    icon: 'ion-ios-pulse-strong',
    markerColor: 'green',
    shape: 'square'
  }),
  animalIcon: L.ExtraMarkers.icon({
    icon: 'ion-ios-paw',
    markerColor: 'green-dark',
    shape: 'square'
  }),
  transportIcon: L.ExtraMarkers.icon({
    icon: 'ion-social-buffer-outline',
    markerColor: 'yellow',
    shape: 'square'
  }),
  carIcon: L.ExtraMarkers.icon({
    icon: 'ion-model-s',
    markerColor: 'yellow',
    shape: 'square'
  }),
  bikeIcon: L.ExtraMarkers.icon({
    icon: 'ion-android-bicycle',
    markerColor: 'green-light',
    shape: 'square'
  }),
  sportIcon: L.ExtraMarkers.icon({
    icon: 'ion-ios-football',
    markerColor: 'green-light',
    shape: 'square'
  }),
  hotelIcon: L.ExtraMarkers.icon({
    icon: 'ion-ios-home-outline',
    markerColor: 'pink',
    shape: 'square'
  }),
  shopIcon: L.ExtraMarkers.icon({
    icon: 'ion-bag',
    markerColor: 'blue-dark',
    shape: 'square'
  })
}
