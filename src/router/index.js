import Vue from "vue";
import Router from "vue-router";

// sync loading
import StadtKatalogOverview from "@/components/StadtKatalogOverview";
import SeestadtBotSearch from "@/components/seestadtbot/SeestadtBotSearch";

const {siteName} = STADTKATALOG_CONFIG;
const isSeestadtBot = siteName === "Seestadt.bot";

// async loading / code splitting
const Entry = () => import("../components/Entry");
const StadtKatalogMap = () =>  import("../components/StadtKatalogMap");
const Privacy = () =>  import("../components/Privacy");
const SeestadtBotPrivacy = () =>  import("../components/seestadtbot/SeestadtBotPrivacy");
const About = () =>  import("@/components/About");
const SeestadtBotHilfe = () =>  import("@/components/seestadtbot/SeestadtBotHelp");
const SeestadtBotAbout = () =>  import("@/components/seestadtbot/SeestadtBotAbout");

// CommonJS syntax for dynamic import
const FrontPage = require(FRONTPAGE_COMPONENT).default;

/**
 * Piwik / Matomo tracking
 */
function trackPageView(url, referrerUrl) {
  if (window._paq) {
    // wait for nextTick + add 250ms delay to
    // give some time to refresh document.title before tracking
    Vue.nextTick(() => {
      setTimeout(() => {
        window._paq.push(
          ['setCustomUrl', url],
          ['setDocumentTitle', document.title]
        );
        if (referrerUrl) {
          window._paq.push(['setReferrerUrl', referrerUrl]);
        }
        window._paq.push(['trackPageView'], ['enableLinkTracking']);
      }, 250);
    });
  }
}

Vue.use(Router);

const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: FrontPage
    },
    {
      path: `${isSeestadtBot ? "/stadtkatalog" : ""}/eintrag/:id`,
      name: "entry",
      component: Entry
    },
    {
      path: `${isSeestadtBot ? "/stadtkatalog" : ""}/ueberblick`,
      name: "sk-overview",
      component: StadtKatalogOverview
    },
    {
      path: `${isSeestadtBot ? "/stadtkatalog" : ""}/karte`,
      name: "sk-map",
      component: StadtKatalogMap
    },
    {
      path: "/ueber",
      name: "about",
      component: isSeestadtBot ? SeestadtBotAbout : About
    },
    {
      path: "/datenschutz",
      name: "privacy",
      component: isSeestadtBot ? SeestadtBotPrivacy : Privacy
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
});

// Special routes for the Seestadt.bot domain
if (siteName === "Seestadt.bot") {
  router.addRoutes([
    {
      path: "/stadtkatalog",
      name: "search",
      component: SeestadtBotSearch
    },
    {
      path: "/hilfe",
      name: "help",
      component: SeestadtBotHilfe,
    },
  ]);
}

let firstRun = true;

router.afterEach((to, from) => {
  trackPageView(to.fullPath, (!firstRun ? document.location + (from.fullPath !== "/" ? from.fullPath : "") : document.referrer));
  firstRun = false;
});

export default router;
