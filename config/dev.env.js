'use strict'

const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  //STK_API_ENDPOINT: '"http://stadtkatalog.test:8000/opendata/v1"'
  STK_API_ENDPOINT: '"https://app.stadtkatalog.org/opendata/v1"'
})
